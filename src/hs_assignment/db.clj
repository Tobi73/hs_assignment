(ns hs-assignment.db)

(def db-name "hs_assignment")

(def db-uri (or (System/getenv "DATABASE_URL")
              "jdbc:postgresql://localhost:5432/"))

(def db-spec (str db-uri db-name))