(ns hs-assignment.changeset
  (:require [clj-time.format :as time-format]
           [clj-time.coerce :as time-coerce]))

(def date-formatter (time-format/formatter "yyyy-MM-dd"))

(def empty-changeset {:valid true :changes {}})

(defn changeset-valid?
    [changeset]
    (boolean (:valid changeset)))

(defn valid-text?
  [text regex]
  (not (nil? (re-matches regex text))))

(defn cast-fields
  [changeset form_data fields]
  (assoc changeset :changes (select-keys form_data fields)))

(defn validate-text-field
  [changeset field_name regex]
  (let [{:keys [changes]} changeset]
    (if (contains? changes field_name)
      (if (valid-text? (field_name changes) regex) changeset (assoc changeset :valid false))
      changeset)))

(defn cast-date
  [changeset field_name]
  (let [{:keys [changes]} changeset]
    (if (contains? changes field_name)
      (assoc-in changeset [:changes field_name] (time-coerce/to-sql-date (time-format/parse date-formatter (field_name changes))))
      changeset)))