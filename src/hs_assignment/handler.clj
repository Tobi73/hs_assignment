(ns hs-assignment.handler
  (:require [hs-assignment.views :as views]
            [hs-assignment.patients.service :as patients]
            [hs-assignment.migrations :as migrations]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.adapter.jetty :as jetty]
            [ring.util.response :as response]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]])
  (:gen-class))

(defroutes app-routes ; replace the generated app-routes with this
  (GET "/"
       []
       (views/home-page))

  (GET "/patients/new"
       []
       (views/new-patient-page))

  (POST "/patients/new"
        {params :params}
        (let [patient_id (patients/create-patient params)]
          (if (not (nil? patient_id))
              (response/redirect (str "/patients/" patient_id))
              (response/redirect "/"))))

  (GET "/patients/:patient_id"
       [patient_id]
       (let [patient (patients/get-patient patient_id)]
          (views/patient-page patient)))

  (GET "/patients/:patient_id/edit"
       [patient_id]
       (let [patient (patients/get-patient patient_id)]
          (views/edit-patient-page patient)))

  (POST "/patients/:patient_id/update"
       {params :params}
       (let [
            patient_id (get params :patient_id)
            patient_id (patients/update-patient patient_id params)
          ]
          (if (not (nil? patient_id))
              (response/redirect (str "/patients/" patient_id))
              (views/new-patient-results-page patient_id))))

  (GET "/patients"
       []
       (views/list-patients-page))

  (POST "/patients/:patient_id/delete"
       [patient_id]
       (patients/delete-patient patient_id)
       (views/list-patients-page))

  (route/resources "/" {:root "public"})
  (route/not-found "Not Found"))

(def app
  (wrap-defaults app-routes site-defaults))

(defn -main
  [& [port]]
  (migrations/migrate)
  (let [port (Integer. (or port
                           (System/getenv "PORT")
                           5000))]
    (jetty/run-jetty #'app {:port  port
                            :join? false})))