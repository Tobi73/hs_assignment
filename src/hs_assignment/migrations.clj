(ns hs-assignment.migrations
 (:require [migratus.core :as migratus]
           [hs-assignment.db :as db]
           [clojure.java.jdbc :as jdbc]))

(def run-config {:store :database
              :migration-dir "migrations/"
              :migration-table-name "migrations"
              :db {:connection-uri db/db-spec}})

(def init-config {:store :database
              :migration-dir "migrations/"
              :init-script "init.sql"
              :init-in-transaction? false
              :migration-table-name "migrations"
              :db {:connection-uri db/db-uri}})

(defn db-already-exists? []
  (-> (jdbc/query db/db-uri
                 [(str "select count(*) from pg_catalog.pg_database "
                       "where datname = 'hs_assignment'")])
      first :count pos?))

(defn migrate
    []
    (when (not (db-already-exists?))
          (migratus/init init-config))
    (migratus/migrate run-config)
    (println " done"))