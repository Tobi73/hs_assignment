(ns hs-assignment.client)

(enable-console-print!)

(defn valid-value?
    [text regex]
    (not (nil? (re-matches regex text))))

(defn handle-valid-value
    [field_name]
    (set! (.. (.getElementById js/document "submit-button") -disabled) false)
    (set! (.. (.getElementById js/document (str field_name "_error")) -style -display) "none"))

(defn handle-invalid-value
    [field_name]
    (set! (.. (.getElementById js/document "submit-button") -disabled) true)
    (set! (.. (.getElementById js/document (str field_name "_error")) -style -display) "block"))

(defn validation-func
    [field_name regex]
    (fn [event]
        (let [new_value (.. event -target -value)]
            (if (valid-value? new_value regex)
                (handle-valid-value field_name)
                (handle-invalid-value field_name)))))
  
(defn load-event []
    (when (not (nil? (js/document.getElementById "edit-patient-page")))
        (.addEventListener (.getElementById js/document "first_name") "input" (validation-func "first_name" #"^[-а-яА-Яр-ю\s]+$") false)
        (.addEventListener (.getElementById js/document "first_name") "change" (validation-func "first_name" #"^[-а-яА-Яр-ю\s]+$") false)

        (.addEventListener (.getElementById js/document "middle_name") "input" (validation-func "middle_name" #"^[-а-яА-Яр-ю\s]+$") false)
        (.addEventListener (.getElementById js/document "middle_name") "change" (validation-func "middle_name" #"^[-а-яА-Яр-ю\s]+$") false)

        (.addEventListener (.getElementById js/document "last_name") "input" (validation-func "last_name" #"^[-а-яА-Яр-ю\s]+$") false)
        (.addEventListener (.getElementById js/document "last_name") "change" (validation-func "last_name" #"^[-а-яА-Яр-ю\s]+$") false)

        (.addEventListener (.getElementById js/document "health_insurance_policy_id") "input" (validation-func "health_insurance_policy_id" #"^\d+$") false)
        (.addEventListener (.getElementById js/document "health_insurance_policy_id") "change" (validation-func "health_insurance_policy_id" #"^\d{16}$") false)))


(js/document.addEventListener  "DOMContentLoaded" load-event)