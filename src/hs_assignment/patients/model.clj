(ns hs-assignment.patients.model
  (:require [clojure.java.jdbc :as jdbc]
            [hs-assignment.db :as db]
            [hs-assignment.changeset :as changeset]))

(def entity-fields [:first_name :middle_name :last_name :birthdate :sex :address :health_insurance_policy_id])

(defn prepare-changeset
  [form_data]
  (-> changeset/empty-changeset
     (changeset/cast-fields form_data entity-fields)
     (changeset/cast-date :birthdate)
     (changeset/validate-text-field :first_name #"^[-а-яА-Яр-ю\s]+$")
     (changeset/validate-text-field :middle_name #"^[-а-яА-Яр-ю\s]+$")
     (changeset/validate-text-field :last_name #"^[-а-яА-Яр-ю\s]+$")
     (changeset/validate-text-field :sex #"^[mf]{1}$")
     (changeset/validate-text-field :health_insurance_policy_id #"^\d{16}$")))

(defn insert-patient
  [{:keys [changes]}]
  (let [results (jdbc/insert! db/db-spec :patients changes)]
    (assert (= (count results) 1))
    (first (vals (first results)))))

(defn get-patient
  [patient_id]
  (jdbc/get-by-id db/db-spec :patients (Integer. patient_id)))

(defn get-all-patients
  []
  (jdbc/query db/db-spec "select * from patients"))

(defn update-patient
  [patient_id {:keys [changes]}]
  (let [results   (jdbc/update! db/db-spec :patients changes ["id = ?" (Integer. patient_id)])]
    (if (= (count results) 1) patient_id nil)))

(defn delete-patient
  [patient_id]
  (jdbc/delete! db/db-spec :patients ["id = ?" (Integer. patient_id)]))