(ns hs-assignment.patients.service
  (:require 
    [hs-assignment.changeset :as changeset]
    [hs-assignment.patients.model :as patient]))

(defn create-patient
  [form_data]
  (let [patient_changeset (patient/prepare-changeset form_data)]
    (if (changeset/changeset-valid? patient_changeset)
      (patient/insert-patient patient_changeset)
      nil)))

(defn update-patient
  [patient_id form_data]
  (let [patient_changeset (patient/prepare-changeset form_data)]
    (if (changeset/changeset-valid? patient_changeset)
      (patient/update-patient patient_id patient_changeset)
      nil)))

(defn get-patient
  [patient_id]
  (patient/get-patient patient_id))

(defn get-all-patients
  []
  (patient/get-all-patients))

(defn delete-patient
  [patient_id]
  (patient/delete-patient patient_id))