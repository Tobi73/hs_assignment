(ns hs-assignment.views
  (:require [hs-assignment.patients.model :as patients-model]
            [clojure.string :as str]
            [hiccup.page :as page]
            [ring.util.anti-forgery :as util]))

(defn gen-page-head
  [title]
  [:head
   [:title (str "Patients: " title)]
   (page/include-css "/css/styles.css")
   (page/include-js "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js")
   (page/include-js "https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.3/jquery.inputmask.min.js")
   (page/include-js "https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.0/bindings/inputmask.binding.js")
   (page/include-js "/js/app.js")])

(def header-links
  [:div#header-links
   "[ "
   [:a {:href "/"} "Домашняя страница"]
   " | "
   [:a {:href "/patients/new"} "Добавить нового пациента"]
   " | "
   [:a {:href "/patients"} "Список пациентов"]
   " ]"])

(defn home-page
  []
  (page/html5
   (gen-page-head "Домашняя страница")
   header-links
   [:h1 "Тестовое задание"]
   [:p "Необходимо собрать свой полный Clojure/ClojureScript
   стек с PostgreSQL и реализовать на нем CRUD пациента (dataset описан ниже). На UI
   должны быть возможности: просмотреть список пациентов, создать, удалить, изменить
   пациента. Желательно с функциональными тестами и CI (сборка по коммиту).
   Подготовить приложение к развертыванию в k8s."]))

(defn text-input
  [id label placeholder value]
  [:div
    [:p (str label ": ") [:input {:id id :type "text" :name id :placeholder placeholder :value value}]]
    [:label {:id (str id "_error") :class "error-label" :for id} "Введенное значение имеет неверный формат!"]])

(defn date-input
  [id label placeholder value]
  [:div
    [:p (str label ": ") [:input {:id id :type "text" :data-inputmask "'mask': '9999-99-99'" :name id :placeholder placeholder :value value}]]
    [:label {:id (str id "_error") :class "error-label" :for id} "Введенное значение имеет неверный формат!"]])

(defn patient-edit
  [patient]
  [:div 
      (text-input "last_name" "Фамилия" "Иванов" (get patient :last_name))
      (text-input "first_name" "Имя" "Иван" (get patient :first_name))
      (text-input "middle_name" "Отчество" "Иванович" (get patient :middle_name))
      [:div {:class "box"}
        [:p "Пол: "
          [:select {:id "sex" :name "sex"}
            [:option {:value "m" :selected (= "m" (get :sex patient))} "Мужской"]
            [:option {:value "f" :selected (= "f" (get :sex patient))} "Женский"]]]]
      (date-input "birthdate" "Дата рождения" "ГГГГ-ММ-ДД" (get patient :birthdate))
      (text-input "address" "Адрес" "" (get patient :address))
      (text-input "health_insurance_policy_id" "Номер полиса ОМС" "" (get patient :health_insurance_policy_id))
      [:p [:input {:type "submit" :id "submit-button" :class "button" :value "Отправить"}]]])

(defn new-patient-page
  []
  (page/html5
   (gen-page-head "Добавление пациента")
   header-links
   [:div {:id "edit-patient-page"}
    [:h1 "Добавление пациента"]
    [:form {:action "/patients/new" :method "POST"}
      (util/anti-forgery-field)
      (patient-edit {})]]))

(defn edit-patient-page
  [patient]
  (page/html5
   (gen-page-head "Редактирование пациента")
   header-links
   [:div {:id "edit-patient-page"}
    [:h1 "Редактирование пациента"]
    [:form {:action (str "/patients/" (:id patient) "/update") :method "POST"}
      (util/anti-forgery-field)
      (patient-edit patient)]]))

(defn new-patient-results-page
  [patient_id]
  (page/html5
     (gen-page-head "Новый пациент добавлен!")
     header-links
     [:h1 "Added new patient!"]
     [:p "Added new patient to the db. "
      [:a {:href (str "/patients/" patient_id)} "See for yourself"]
      "."]))

(defn patient-page
  [{:keys [id first_name middle_name last_name birthdate sex address health_insurance_policy_id]}]
  (page/html5
     (gen-page-head (str "Пациент " id))
     header-links
     [:h1 {:id "test-id"} "Пациент"]
     [:p "ID: " id]
     [:p "Имя: " first_name]
     [:p "Отчество: " middle_name]
     [:p "Фамилия: " last_name]
     [:p "Дата рождения: " birthdate]
     [:p "Пол: " (if (= "m" sex) "Мужской" "Женский")]
     [:p "Адрес: " address]
     [:p "Номер полиса ОМС: " health_insurance_policy_id]
     [:a {:href (str "/patients/" id "/edit") :class "button"} "Edit"]
     [:form {:action (str "/patients/" id "/delete") :method "POST"} 
        (util/anti-forgery-field)
        [:input {:type "submit" :class "button-delete" :value "Delete"}]]))


(defn list-patients-page
  []
  (let [patients (patients-model/get-all-patients)]
    (page/html5
     (gen-page-head "Пациенты")
     header-links
     [:h1 "Пациенты"]
     [:table
      [:tr 
        [:th "ID"] 
        [:th "Имя"] 
        [:th "Отчество"]
        [:th "Фамилия"]]
      (for [patient patients]
        (let [
            patient_id (:id patient)
            first_name (:first_name patient)
            middle_name (:middle_name patient)
            last_name (:last_name patient)
          ]
            [:tr 
              [:td [:a {:href (str "/patients/" patient_id)} patient_id]]
              [:td first_name]
              [:td middle_name]
              [:td last_name]]))])))