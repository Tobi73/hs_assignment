(defproject hs-assignment "0.1.0"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/clojurescript "1.10.764"]
                 [compojure "1.6.1"]
                 [ring/ring-jetty-adapter "1.5.1"]
                 [ring/ring-defaults "0.3.2"]
                 [org.clojure/java.jdbc "0.6.1"]
                 [org.postgresql/postgresql "9.4-1201-jdbc41"]
                 [clj-time "0.15.2"]
                 [migratus "1.2.8"]]
  :plugins [
    [migratus-lein "0.7.3"]
    [lein-cljsbuild "1.1.8"]]
  :main hs-assignment.handler
  :uberjar-name "hs_assignment-standalone.jar"
  :ring {:handler hs-assignment.handler/app
         :init hs-assignment.migrations/migrate}

  :resource-paths ["resources" "target/cljsbuild"]

  :cljsbuild
  {:builds {:min
            {:source-paths ["src"]
             :compiler
             {:output-to        "target/cljsbuild/public/js/app.js"
              :output-dir       "target/cljsbuild/public/js"
              :source-map       "target/cljsbuild/public/js/app.js.map"
              :optimizations :advanced
              :infer-externs true
              :pretty-print  false}}
            :app
            {:source-paths ["src"]
             :figwheel {:on-jsload "hs-patients.core/mount-root"}
             :compiler
             {:main "hs-assignment.client"
              :asset-path "/js/out"
              :output-to "target/cljsbuild/public/js/app.js"
              :output-dir "target/cljsbuild/public/js/out"
              :source-map true
              :optimizations :none
              :pretty-print  true}}
            }
   }

  :figwheel {:http-server-root "public"
              :css-dirs ["resources/public/css"]
              :ring-handler hs-assignment.handler/app}
            
  :migratus {:store                :database
              :migration-dir        "migrations/"
              :init-script          "init.sql"
              :init-in-transaction? false
              :migration-table-name "migrations"
              :db (or (System/getenv "DATABASE_CONNECTION_STRING") "postgresql://localhost:5432/hs_assignment")}
              
  :profiles {:dev {:source-paths ["src"]
                  :plugins [
                    [lein-ring "0.12.5"]
                    [lein-figwheel "0.5.20"]]
                  :env {:dev true}}
            :test {:source-paths ["src" "test"]
                  :dependencies [
                    [ring/ring-mock "0.4.0"]
                  ]
                  :env {:dev true}}
            :uberjar {:source-paths ["src"]
                  :prep-tasks ["compile" ["cljsbuild" "once" "min"]]
                  :env {:production true}
                  :aot :all
                  :omit-source true}})

