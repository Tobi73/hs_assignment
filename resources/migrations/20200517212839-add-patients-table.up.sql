CREATE TABLE IF NOT EXISTS public.patients
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    first_name character varying(255) COLLATE pg_catalog."default",
    middle_name character varying(255) COLLATE pg_catalog."default",
    last_name character varying(255) COLLATE pg_catalog."default",
    sex character varying(1) COLLATE pg_catalog."default",
    address character varying(255) COLLATE pg_catalog."default",
    health_insurance_policy_id character varying(16) COLLATE pg_catalog."default",
    birthdate date,
    CONSTRAINT patients_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
