(ns hs-assignment.handler-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [hs-assignment.handler :refer :all]))

(deftest test-app
  (def test-patient {
    :first_name "Андрей" 
    :middle_name "Олегович" 
    :last_name "Зайцев" 
    :birthdate "1997-04-3"
    :sex "m"
    :address "Ульяновск" 
    :health_insurance_policy_id "11111111111111111111"})

  (testing "Home page GET /"
    (let [response (app (mock/request :get "/"))]
      (is (= (:status response) 200))
      (is (.contains (:body response) "Тестовое задание"))
      (is (.contains (:body response) "Необходимо собрать свой полный Clojure/ClojureScript"))))

  (testing "List of patients GET /patients"
    (let [response (app (mock/request :get "/patients"))]
      (is (= (:status response) 200))
      (is (.contains (:body response) "Пациенты"))
      (is (.contains (:body response) "<table>"))
      (is (.contains (:body response) "<th>ID</th>"))
      (is (.contains (:body response) "<th>Имя</th>"))
      (is (.contains (:body response) "<th>Отчество</th>"))
      (is (.contains (:body response) "<th>Фамилия</th>"))))

  (testing "New patient page GET /patients/new"
    (let [response (app (mock/request :get "/patients/new"))]
      (is (= (:status response) 200))
      (is (.contains (:body response) "Добавление пациента"))
      (is (.contains (:body response) "Имя: "))
      (is (.contains (:body response) "Фамилия: "))
      (is (.contains (:body response) "Отчество: "))
      (is (.contains (:body response) "Пол: "))
      (is (.contains (:body response) "Дата рождения: "))
      (is (.contains (:body response) "Адрес: "))
      (is (.contains (:body response) "Номер полиса ОМС: "))))

  (testing "not-found route"
    (let [response (app (mock/request :get "/invalid"))]
      (is (= (:status response) 404)))))
